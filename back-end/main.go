package main

import (
	"fmt"
	"log"

	"bitbucket.org/camiloaromero23/chinauta-accounting/src/main/back-end/routes"
	"bitbucket.org/camiloaromero23/chinauta-accounting/src/main/back-end/storage"
	"bitbucket.org/camiloaromero23/chinauta-accounting/src/main/back-end/utils"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func main() {
	PORT := fmt.Sprintf(":%s", utils.GetEnv("BACKEND_PORT", "5000"))

	dc := storage.DbConfig{
		USERNAME: utils.GetEnv("DB_USERNAME", "root"),
		PASSWORD: utils.GetEnv("DB_PASSWORD", "root"),
		HOST:     utils.GetEnv("DB_HOST", "localhost"),
		PORT:     utils.GetEnv("DB_PORT", "3306"),
		NAME:     utils.GetEnv("DB_NAME", "chinauta_db"),
	}

	driver := storage.MySQL

	storage.New(driver, dc)

	app := fiber.New()

	app.Use(logger.New())

	app.Use(cors.New(cors.Config{
		AllowOrigins: utils.GetEnv("CLIENT_ORIGIN", "http://localhost:8100"),
		AllowHeaders: "Authorization, Origin, Content-Type",
		AllowMethods: "GET,POST,PUT,DELETE",
	}))

	routes.PublicRoutes(app)
	routes.PrivateRoutes(app)

	// if err := app.ListenTLS(PORT, "./certs/cert.pem", "./certs/key.pem"); err != nil {
	// 	log.Panic(err)
	// }

	if err := app.Listen(PORT); err != nil {
		log.Panic(err)
	}
}
