module bitbucket.org/camiloaromero23/chinauta-accounting/src/main/back-end

go 1.16

require (
	github.com/form3tech-oss/jwt-go v3.2.2+incompatible
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gofiber/fiber/v2 v2.7.1
	github.com/gofiber/jwt/v2 v2.2.1
	github.com/klauspost/compress v1.11.13 // indirect
	github.com/valyala/fasthttp v1.23.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.6
)
