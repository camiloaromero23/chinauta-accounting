# Chinauta Accounting app back-end

## Environment variables

``` env
DB_PASSWORD
DB_PORT
```

## Generate key and certificate for TLS

``` console
mkdir certs

openssl req -x509 -newkey rsa:4096 -keyout ./certs/key.pem -out ./certs/cert.pem -days 365 -nodes
```

## Run development environment

Start MySQL docker container

``` console
docker-compose up -d
```

Start nodemon for live reloading

``` console
nodemon
```
