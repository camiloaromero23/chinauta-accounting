package storage

import (
	"fmt"
	"log"
	"sync"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	db   *gorm.DB
	once sync.Once
)

type Driver string

type DbConfig struct {
	USERNAME,
	PASSWORD,
	HOST,
	PORT,
	NAME string
}

const (
	MySQL Driver = "MYSQL"
)

func New(d Driver, dc DbConfig) {
	switch d {
	case MySQL:
		newMySQLDB(dc)
	default:
		log.Panicf("%v not supported yet", d)
	}
}

func newMySQLDB(dc DbConfig) {
	once.Do(func() {
		var err error
		dsn := fmt.Sprintf(
			"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
			dc.USERNAME,
			dc.PASSWORD,
			dc.HOST,
			dc.PORT,
			dc.NAME)
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
		if err != nil {
			log.Fatalf("Can not open MySQL DB: %v", err)
		}

		fmt.Println("Connected to MySQL")
	})
}

func DB() *gorm.DB {
	return db
}
