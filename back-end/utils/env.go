package utils

import "os"

func GetEnv(key string, def string) string {
	if val, found := os.LookupEnv(key); found {
		return val
	}
	return def
}
