package routes

import (
	"bitbucket.org/camiloaromero23/chinauta-accounting/src/main/back-end/controllers"
	"github.com/gofiber/fiber/v2"
)

func AuthRoutes(a *fiber.App) {
	router := a.Group("api/auth")
	router.Post("/login", controllers.Login)
	router.Post("/signUp", controllers.SignUp)
}
