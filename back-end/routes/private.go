package routes

import (
	"bitbucket.org/camiloaromero23/chinauta-accounting/src/main/back-end/controllers"
	"bitbucket.org/camiloaromero23/chinauta-accounting/src/main/back-end/utils"
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v2"
)

func PrivateRoutes(a *fiber.App) {
	router := a.Group("/api")

	router.Use(jwtware.New(jwtware.Config{
		ErrorHandler: func(c *fiber.Ctx, e error) error {
			return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
				"error": "Unauthorized",
			})
		},
		SigningKey: utils.GetEnv("SECRET", "secret"),
	}))

	router.Get("/helloworld", controllers.HelloWorld)
}
