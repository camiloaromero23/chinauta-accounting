package controllers

import "github.com/gofiber/fiber/v2"

func HelloWorld(c *fiber.Ctx) error {
	return c.Status(fiber.StatusOK).JSON(&fiber.Map{
		"error":   false,
		"message": "Hello World from fiber!!!",
	})
}
